import React from 'react';
import './App.css';
import { Tabs, TabList, Tab, PanelList, Panel } from '../Tabs';

export function App() {
  return (
    <div className="App">
			<Tabs>
				<TabList>
					<Tab>One</Tab>
					<Tab>Two</Tab>
					<Tab>Three</Tab>
				</TabList>
				<PanelList>
					<Panel>One Panel</Panel>
					<Panel>Two Panel</Panel>
					<Panel>Three Panel</Panel>
				</PanelList>
			</Tabs>
    </div>
  );
}
