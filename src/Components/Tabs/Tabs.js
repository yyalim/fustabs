import React, { Component } from "react";

export class Tabs extends Component {
	state = {
		activeIndex: 0
	}

	selectTabIndex = (activeIndex) => {
		this.setState({ activeIndex });
	}

	render() {
		const { state, props } = this;
		const children = React.Children.map(props.children, (child) => (
			React.cloneElement(child, {
				activeIndex: state.activeIndex,
				onSelectIndex: this.selectTabIndex
			})
		));

		return <div className="tabs">{children}</div>;
	}
}
