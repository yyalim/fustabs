import React, { Component } from 'react';
import './Tabs.css';

export class Tab extends Component {
	clickHandler = () => {
		const {index, onSelectIndex} = this.props;

		onSelectIndex(index);
	};

	render() {
		const { isActive, isDisabled, children } = this.props;
		const classNames = `tab ${isActive ? 'active' : ''} ${isDisabled ? 'disabled' : ''}`;

		return (
			<div className={classNames} onClick={this.clickHandler}>
				{children}
			</div>
		)
	}
}
