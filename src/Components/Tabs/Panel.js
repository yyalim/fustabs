import React from 'react';

export const Panel = ({children}) => (
	<div className="panel">
		{children}
	</div>
);
