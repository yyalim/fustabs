import React from "react";

export function PanelList({ activeIndex, children }) {
  const panels = React.Children.map(children, (child, index) => (
		index === activeIndex
			? React.cloneElement(child)
			: null
	));

  return <div className="panel-list">{panels}</div>;
}
