import React, { Component } from 'react';

export class TabList extends Component {

	onSelectIndex = (index) => {
		const { onSelectIndex } = this.props;

		onSelectIndex(index);
	}


	render() {
		const { children, activeIndex } = this.props;

		const tabs = React.Children.map(children, (child, index) => (
			React.cloneElement(child, {
				isActive: activeIndex === index,
				onSelectIndex: this.onSelectIndex,
				index
			})
		));

		return <div className="tab-list">{tabs}</div>;
	}
}